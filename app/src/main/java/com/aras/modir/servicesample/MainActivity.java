package com.aras.modir.servicesample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.start);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sampleService = new Intent(MainActivity.this, SampleService.class);
//                Bundle b = new Bundle();
//                b.putString("name", "Amir");
//                sampleService.putExtra("name: " , b);
                sampleService.putExtra("url", "urllll");
                startService(sampleService);
            }
        });

    }
}
